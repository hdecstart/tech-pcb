package com.pcb.api.controller;

import java.util.List;
import com.pcb.api.util.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pcb.api.bean.Operation;
import com.pcb.api.model.entity.TC;
import com.pcb.api.model.service.IProcessTCService;
import com.pcb.api.request.UserRequest;
import com.pcb.api.response.ResponseFormat;

@RestController
@RequestMapping("/change")
public class MoneyExchangeController {
	
	@Autowired
	IProcessTCService service;

	@GetMapping("get-public-tc")
	public ResponseEntity<ResponseFormat<Operation>> Test(@RequestBody UserRequest request) {
		try {
			ResponseFormat<Operation> response = new ResponseFormat<Operation>();
			response.setStatus(Constants.OK);
			response.setStatusMessage(Constants.MSG_OK);
			response.setData(service.loadTc(request));
			return new ResponseEntity<ResponseFormat<Operation>>(response, HttpStatus.OK);	
		} catch (Exception e) {
			ResponseFormat<Operation> response = new ResponseFormat<Operation>();
			response.setStatus(Constants.NO_OK);
			response.setStatusMessage(e.getMessage());
			response.setData(null);
			return new ResponseEntity<ResponseFormat<Operation>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("historyTC")
	public ResponseEntity<ResponseFormat<List<TC>>> Test1(){
		try {
			ResponseFormat<List<TC>> response = new ResponseFormat<>();
			response.setStatus(Constants.OK);
			response.setStatusMessage(Constants.MSG_OK);
			response.setData(service.getHistoryTC());
			return new ResponseEntity<ResponseFormat<List<TC>>>(response,HttpStatus.OK);
		} catch (Exception e) {
			ResponseFormat<List<TC>> response = new ResponseFormat<>();
			response.setStatus(Constants.NO_OK);
			return new ResponseEntity<ResponseFormat<List<TC>>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("save-tc")
	public ResponseEntity<ResponseFormat<TC>> Test2(@RequestBody TC obj){
		try {
			ResponseFormat<TC> response = new ResponseFormat<>();
			response.setStatus(Constants.OK);
			response.setStatusMessage(Constants.MSG_OK);
			response.setData(service.saveTC(obj));
			return new ResponseEntity<ResponseFormat<TC>>(response,HttpStatus.OK);
		} catch (Exception e) {
			ResponseFormat<TC> response = new ResponseFormat<>();
			response.setStatus(Constants.NO_OK);
			return new ResponseEntity<ResponseFormat<TC>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
