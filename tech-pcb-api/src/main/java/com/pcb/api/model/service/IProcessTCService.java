package com.pcb.api.model.service;

import java.util.List;

import com.pcb.api.bean.Exchange;
import com.pcb.api.bean.Operation;
import com.pcb.api.model.entity.TC;

public interface IProcessTCService {
	public Operation loadTc(Exchange exchange) throws Exception;
	public List<TC> getHistoryTC();
	public TC getTCAvailable();
	public TC saveTC(TC tc) throws Exception;
}
