package com.pcb.api.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pcb.api.bean.Exchange;
import com.pcb.api.bean.Operation;
import com.pcb.api.model.entity.TC;
import com.pcb.api.model.service.IProcessTCService;
import com.pcb.api.repository.ProcessTCRepository;
import com.pcb.api.util.Constants;


@Service
public class ProcessTCServiceImpl implements IProcessTCService{

	@Autowired
	ProcessTCRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public Operation loadTc(Exchange exchange) throws Exception{
		Operation ope = new Operation();
		
		TC tc = new TC();
		tc = repository.findByDisponibilityAndName(true, exchange.getMonedaDestino());
		if ( tc != null ) {
			ope.setMonedaOrigen(exchange.getMonedaOrigen());
			ope.setMonedaDestino(tc.getName());
			ope.setMonto(exchange.getMonto());
			ope.setTipoCambio(tc.getPrice());
		}else {
			throw new Exception(Constants.MSG_INVALID_MONEY);
		}

		return ope;
	}

	@Override
	@Transactional(readOnly = true)
	public List<TC> getHistoryTC() {
		return repository.findByOrderByIdDesc();
	}

	@Override
	@Transactional(readOnly = true)
	public TC getTCAvailable() {
		return repository.findTopByOrderByIdDesc();
	}

	@Override
	@Transactional
	public TC saveTC(TC tc) throws Exception {	
		TC tcResult = null;
		TC tcNew = new TC();
		tcNew.setId(0);
		tcNew.setName(tc.getName());
		tcNew.setDisponibility(true);
		tcNew.setPrice(tc.getPrice());
		tcResult = repository.save(tcNew);
		
		if (tcResult != null) {
			TC tcPre = repository.findById(tc.getId()).get();
			tc.setDisponibility(false);
			tc.setPrice(tcPre.getPrice());
			
			if (repository.save(tc) == null)
				throw new Exception(Constants.MSG_ERROR_UPDATE_TC);
		}else {
			throw new Exception(Constants.MSG_ERROR_SAVE_TC);	
		}
		
		return tcResult;
	}

}
