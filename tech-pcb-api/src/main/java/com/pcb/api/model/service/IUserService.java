package com.pcb.api.model.service;

import com.pcb.api.model.entity.User;

public interface IUserService {
	public User findUser(String user, String pass);
}
