package com.pcb.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechPcbApiRestApplication{

	public static void main(String[] args) {
		SpringApplication.run(TechPcbApiRestApplication.class, args);
	}

}
