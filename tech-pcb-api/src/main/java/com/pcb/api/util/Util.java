package com.pcb.api.util;

import java.text.DecimalFormat;

public class Util {
	public static float formatMoney(float monto) {
		return Float.parseFloat(new DecimalFormat("#.00").format(monto));
	}
}
