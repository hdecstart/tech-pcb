package com.pcb.api.util;

public class Constants {
	public static final short OK = 0;
	public static final short NO_OK = -1;
	public static final String MSG_OK = "Success";
	public static final String MSG_INTERNAL_ERROR = "Error interno";
	public static final String MSG_INVALID_MONEY = "Moneda ingresada no es válida";
	public static final String MSG_ERROR_UPDATE_TC = "Error al actualizar TC";
	public static final String MSG_ERROR_SAVE_TC = "Error al grabar nuevo TC";
}
