package com.pcb.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcb.api.model.entity.User;

public interface UserRepository  extends JpaRepository<User, Long>{
	public User findByUserName(String user);
}
