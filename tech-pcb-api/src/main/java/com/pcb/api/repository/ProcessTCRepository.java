package com.pcb.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcb.api.model.entity.TC;

public interface ProcessTCRepository extends JpaRepository<TC, Long>{
	public TC findTopByOrderByIdDesc();
	public TC findByDisponibilityAndName(boolean available, String moneyType);
	public List<TC> findByOrderByIdDesc();
}
