package com.pcb.api.bean;

import static com.pcb.api.util.Util.formatMoney;

public class Operation extends Exchange{
	private float montoConTC;
	private float tipoCambio;
	
	public float getMontoConTC() {
		this.montoConTC = formatMoney(super.getMonto() / this.getTipoCambio());
		return montoConTC;
	}
	public float getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(float tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
}
