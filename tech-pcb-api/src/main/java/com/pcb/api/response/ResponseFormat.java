package com.pcb.api.response;

public class ResponseFormat<T> {
	private int status; //Codigo funcional 1 ok - -1 error
	private T data;
	private String statusMessage; //Mensaje funcional - Mensajes de exito, error de validación o problemas internos
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
	
	
	
}
