insert into USERS(user, pass) values ('user001','$2a$10$NhEWIUE6UUlMQUKQLmA0Me45qTIlRa.4XpGQyU03LDJunXd51YwWy');

insert into TC(money_type, price, available,last_register_date) values ('USD',3.95,false,NOW());
insert into TC(money_type, price, available,last_register_date) values ('USD',3.98,true,NOW());
insert into TC(money_type, price, available,last_register_date) values ('EUR',4.75,false,NOW());
insert into TC(money_type, price, available,last_register_date) values ('EUR',4.85,true,NOW());