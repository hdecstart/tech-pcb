import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TemplateService {
private url:string = "http://localhost:8080/change/historyTC";

  constructor(
    private http : HttpClient
  ) { }

  loadHistory():Observable<any>{
      return this.http.get(this.url);
  }
}