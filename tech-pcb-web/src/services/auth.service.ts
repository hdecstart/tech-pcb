import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
private url:string = "http://localhost:8080/oauth/token";
private credentials:string = btoa(`pcb-web:Exch@nge`);

  private httpHeaders = new HttpHeaders({
    "Content-Type": "application/x-www-form-urlencoded",
    'Authorization' : 'Basic ' + this.credentials
  });

  constructor(
    private http : HttpClient
  ) { }

  login(params:object):Observable<any>{
      console.log(params);
      
      let obj = new URLSearchParams();
      obj.set('grant_type', 'password');
      obj.set('username', params['userName'].value);
      obj.set('password', params['contrasena'].value);
      return this.http.post(this.url, obj.toString(), { headers :this.httpHeaders});
  }
}
