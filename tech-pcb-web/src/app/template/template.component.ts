import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { TemplateService } from 'src/services/template.service';
import { TC } from 'src/models/tc';


@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

    columns:string[] = ["id","Moneda de Origen","Moneda de destino", "Precio de venta", "Disponible", "Fecha de Registro"];
    tcData:TC[];

    constructor(
        private service:TemplateService
      ) { }
    
      ngOnInit(): void {
        this.loadHistory();
      }

      loadHistory(){
          this.service.loadHistory()
          .subscribe((resp:any) => {
                this.tcData = resp['data'];
                console.log(this.tcData);
                
          })
      }
}