import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private userName:string;
  private contrasena:string;
  formGroup: FormGroup;

  formLogin: FormGroup = this.fb.group({
    userName: ['', [Validators.required, Validators.maxLength(50)]],
    contrasena: ['', [Validators.required,Validators.maxLength(70)]],
  });

  constructor(private fb: FormBuilder,
    private service: AuthService,
    private router:Router) {
  }

  ngOnInit(): void {
  }

  onValidate(){
    if (!this.formLogin.valid){
      Swal.fire("Error","Datos invalidos","error");
    }else{
      let obj = {
        
      }
      this.service.login(this.formLogin.controls)
      .subscribe(resp => {
        console.log(resp);
        this.router.navigate(['/productos']);
        Swal.fire("ok","Bienvenid@!","success");
      }, err => {
        Swal.fire("Error",err.error.errorMessage,"error");
      })
    }
  }

}
